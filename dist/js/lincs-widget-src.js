//Set up and imports
const lincs_winkNLP = require('wink-nlp');
const lincs_model = require('wink-eng-lite-web-model');
const lincs_its = require( 'wink-nlp/src/its.js' );
const lincs_nlp = lincs_winkNLP(lincs_model);
const lincs_patterns_1 = [
    {
        name: 'tripleNoun',
        patterns: ['[NOUN] [NOUN] [NOUN]']
    },
    {
        name: 'triplePropn',
        patterns: ['[PROPN] [PROPN] [PROPN]']
    }
];
const lincs_patterns_2 = [
    {
        name: 'doubleNoun',
        patterns: ['[NOUN] [NOUN]']
    },
    {
        name: 'doublePropn',
        patterns: ['[PROPN] [PROPN]']
    }
];
const lincs_patterns_3 = [
    {
        name: 'single',
        patterns: ['[NOUN|PROPN]']
    }
];
const lincs_patterns_4 = [
    {
        name: 'tripleNoun',
        patterns: ['[DET] [ADJ] [NOUN]']
    },
    {
        name: 'triplePropn',
        patterns: ['[DET] [ADJ] [PROPN]']
    }
];
const lincs_patterns_5 = [
    {
        name: 'doubleNoun',
        patterns: ['[DET|ADJ] [NOUN]']
    },
    {
        name: 'doublePropn',
        patterns: ['[DET|ADJ] [PROPN]']
    }
];
const lincs_patterns_6 = [
    {
        name: 'tripleMix',
        patterns: ['[NOUN|PROPN] [NOUN|PROPN] [NOUN|PROPN]']
    }
];
const lincs_patterns_7 = [
    {
        name: 'doubleMix',
        patterns: ['[NOUN|PROPN] [NOUN|PROPN]']
    }
];

const entityCard = { // A card containing a word and a drop down with it's related nodes
    template:
    /*html*/
    `
    <div class="entityCard">
        <div class="entityCard-header" style="width: 100%;" v-on:click="showInfo()">
            <h1 style="text-align: center;">{{ entity }}</h1>
        </div>
        <transition name="expand">
            <div v-show="toggle" class="expansion-div">
                <table>
                    <tr>
                        <th>Literal</th>
                        <th>URI</th>
                    </tr>
                    <tr v-for="result in results">
                        <td style="padding-right: 10px;">{{ result.literal }}</td>
                        <td><a target="_blank" :href="'https://rs.stage.lincsproject.ca/resource/?uri=' + result.uri">{{ result.uri }}</a></td>
                    </tr>
                </table>
            </div>
        </transition>
    </div>
    `,


    props: {
        entity: {
            type: String,
            required: true
        }
    },


    data() {
        return {
            toggle: false,
            results: []
        }
    },


    methods: {
        showInfo() { // Get nodes related to the entity card's word
            if (this.results.length == 0) {
                var results = this.results;
                var xhr = new XMLHttpRequest();
                xhr.open("GET", "https://fuseki.lincsproject.ca/lincs/sparql?" +
                                "query=PREFIX+text%3A+%3Chttp%3A%2F%2Fjena.apache.org%2Ftext%23%3" +
                                "E%0APREFIX+rdfs%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-" +
                                "schema%23%3E%0ASELECT+distinct+%3Fs+%3Fliteral+WHERE+%7B%0A++++V" +
                                "ALUES+(%3FsearchTerm)+%7B%0A++++(%22" + encodeURI(this.entity) + 
                                "%22)%0A++++%7D%0A++(%3Fs+%3Fscore+%3Fliteral+%3Fg)+text%3Aquery+" +
                                "(rdfs%3Alabel+%3FsearchTerm)+.%0A++filter+regex(str(%3Fs)%2C+%22" +
                                "%5Ehttp%3A%2F%2Fid.lincsproject.ca%2F%22%2C+%22i%22)+.%0A%7D%0A+", true);
                xhr.setRequestHeader("accept", "application/sparql-results+xml");
                xhr.send(null);

                xhr.onload = function(e) {
                    var doc = new DOMParser().parseFromString(this.responseText, 'text/xml');
                    var uris = doc.getElementsByTagName("uri");
                    var literals = doc.getElementsByTagName("literal");

                    for (var i = 0; i < uris.length; i++) {
                        if (literals[i].parentNode.getAttribute("name") == "literal") {
                            results.push({"literal": literals[i].textContent,"uri": uris[i].textContent});
                        }
                    }
                }
            }

            this.toggle = !this.toggle;
        }
    }
};

const app = Vue.createApp({
    data() {
        return {
            search: "",
            entities: [],
            displayedEntities: [],
            singleNouns: [],
            doubleNouns: [],
            tripleNouns: []
        }
    },


    methods: {
        applyTextFilter() { // Apply a string filter to the entity cards
            var search = this.search;
            if (search == "") {
                this.displayedEntities = this.entities;
                return;
            }

            function filterCompare(value) {
                if (value.word.toUpperCase().includes(search.toUpperCase())) {
                    return true;
                }
                return false;
            }

            this.displayedEntities = this.entities.filter(filterCompare);
        },
        addTags(tag) { // Add a word to the list of entity cards
            if (tag["value"].replace(".", "").length <= 1) {
                return;
            } else if (this.singleNouns.findIndex(word => tag["value"].toLowerCase() === word.toLowerCase()) != -1) {
                return;
            } else if (this.doubleNouns.findIndex(word => tag["value"].toLowerCase() === word.toLowerCase()) != -1) {
                return;
            } else if (this.tripleNouns.findIndex(word => tag["value"].toLowerCase() === word.toLowerCase()) != -1) {
                return;
            } 

            switch(tag["type"]) {
                case "single":
                    if (!this.singleNouns.includes(tag["value"])) {
                        this.singleNouns.push(tag["value"]);
                    }
                    break;
                case "doubleNoun":
                    if (!this.doubleNouns.includes(tag["value"])) {
                        this.doubleNouns.push(tag["value"]);
                    }
                    break;
                case "doublePropn":
                    if (!this.doubleNouns.includes(tag["value"])) {
                        this.doubleNouns.push(tag["value"]);
                    }
                    break;
                case "doubleMix":
                    if (!this.doubleNouns.includes(tag["value"])) {
                        this.doubleNouns.push(tag["value"]);
                    }
                    break;
                case "tripleNoun":
                    if (!this.tripleNouns.includes(tag["value"])) {
                        this.tripleNouns.push(tag["value"]);
                    }
                    break;
                case "triplePropn":
                    if (!this.tripleNouns.includes(tag["value"])) {
                        this.tripleNouns.push(tag["value"]);
                    }
                    break;
                case "tripleMix":
                    if (!this.tripleNouns.includes(tag["value"])) {
                        this.tripleNouns.push(tag["value"]);
                    }
                    break;
            }
        }
    },


    mounted: async function() { // Find word tags and query the ts to see if they exist
        var _this = this;

        document.getElementById("entity-scroll-container").style.height = (window.innerHeight - 212) + "px";

        window.onmessage = function(e) {
            var text = e.data;

            lincs_nlp.learnCustomEntities(lincs_patterns_1);
            var tags1 = lincs_nlp.readDoc(text);
            tags1.customEntities().out(lincs_its.detail).forEach(function(tag) {
                _this.addTags(tag);
            });

            lincs_nlp.learnCustomEntities(lincs_patterns_2);
            var tags2 = lincs_nlp.readDoc(text);
            tags2.customEntities().out(lincs_its.detail).forEach(function(tag) {
                _this.addTags(tag);
            });

            lincs_nlp.learnCustomEntities(lincs_patterns_3);
            var tags3 = lincs_nlp.readDoc(text);
            tags3.customEntities().out(lincs_its.detail).forEach(function(tag) {
                _this.addTags(tag);
            });

            lincs_nlp.learnCustomEntities(lincs_patterns_4);
            var tags4 = lincs_nlp.readDoc(text);
            tags4.customEntities().out(lincs_its.detail).forEach(function(tag) {
                _this.addTags(tag);
            });

            lincs_nlp.learnCustomEntities(lincs_patterns_5);
            var tags5 = lincs_nlp.readDoc(text);
            tags5.customEntities().out(lincs_its.detail).forEach(function(tag) {
                _this.addTags(tag);
            });

            lincs_nlp.learnCustomEntities(lincs_patterns_6);
            var tags6 = lincs_nlp.readDoc(text);
            tags6.customEntities().out(lincs_its.detail).forEach(function(tag) {
                _this.addTags(tag);
            });

            lincs_nlp.learnCustomEntities(lincs_patterns_7);
            var tags7 = lincs_nlp.readDoc(text);
            tags7.customEntities().out(lincs_its.detail).forEach(function(tag) {
                _this.addTags(tag);
            });

            var lincs_numRec = 0;
            function lincsReqLinks(i, j, nouns, nouns2, nouns3) {
                if (nouns == null) {
                    lincsReqLinks(0, nouns2.length, nouns2, nouns3, null);
                } else if (nouns == null && nouns2 == null && nouns3 == null) {
                    return;
                }
                var temp, chunk = 10;

                setTimeout(function() {
                    temp = nouns.slice(i, i+chunk);

                    var search = "";
                    temp.forEach(function(term) {
                        search += "(%22" + encodeURI(term) + "%22)%0A++++";
                    });

                    var xhr = new XMLHttpRequest();
                    xhr.open("GET", "https://fuseki.lincsproject.ca/lincs/sparql?" +
                                    "query=PREFIX+text%3A+%3Chttp%3A%2F%2Fjena.apache.org%2Ftext%23%3" +
                                    "E%0APREFIX+rdfs%3A+%3Chttp%3A%2F%2Fwww.w3.org%2F2000%2F01%2Frdf-" +
                                    "schema%23%3E%0A%0ASELECT+distinct+%3FsearchTerm+WHERE+%7B%0A++++" +
                                    "VALUES+(%3FsearchTerm)+%7B%0A++++" + search + "%7D%0A++(%3Fs+%3F" +
                                    "score+%3Fliteral+%3Fg)+text%3Aquery+(rdfs%3Alabel+%3FsearchTerm)" +
                                    "+.%0A++filter+regex(str(%3Fs)%2C+%22%5Ehttp%3A%2F%2Fid.lincsproj" +
                                    "ect.ca%2F%22%2C+%22i%22)+.%0A++filter+regex(str(%3Fliteral)%2C+c" +
                                    "oncat(%22%5C%5Cb%22%2C%3FsearchTerm%2C%22%5C%5Cb%22)%2C+%22i%22)" +
                                    "+.%0A%7D%0A+", true);
                    xhr.setRequestHeader("accept", "application/sparql-results+xml");
                    xhr.send(null);

                    xhr.onload = function(e) {
                        var doc = new DOMParser().parseFromString(this.responseText, 'text/xml');
                        var words = doc.getElementsByTagName("literal");

                        for (var x=0, max=words.length; x < max; x++) {
                            _this.entities.push({"card": entityCard, "word": words[x].textContent});
                            _this.displayedEntities.push({"card": entityCard, "word": words[x].textContent});
                        }

                        lincs_numRec += 1;
                        if (lincs_numRec == Math.ceil(nouns.length / chunk)) {
                            if (nouns3 != null) {
                                lincs_numRec = 0;
                                lincsReqLinks(0, nouns2.length, nouns2, nouns3, null);
                            } else if (nouns2 != null) {
                                lincs_numRec = 0;
                                lincsReqLinks(0, nouns2.length, nouns2, null, null);
                            } else {
                                lincs_numRec = 0;
                            }
                        }
                    }
                    if ((i += chunk) && i < j) lincsReqLinks(i, j, nouns, nouns2, nouns3);
                }, 100);
            }

            lincsReqLinks(0, _this.tripleNouns.length, _this.tripleNouns, _this.doubleNouns, _this.singleNouns);
        }
    }
});

app.mount("#app");