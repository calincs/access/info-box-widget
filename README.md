# Info Box Widget

This is a widget that can be built into any website. The Lincs Entity Widget scans the entire page for text and displays related entities that it finds within the Research Space triple store.

## Installation

The widget can be installed on any html page, or website builder that supports script tags.

## HTML Installation

Simply copy and paste the following script tag onto the html page wherever you'd like the widget to display. To modify the display style, you can add css styles to the id lincs-widget (#lincs-widget { }).

```
<script id="lincs-widget-setup">
    window.addEventListener('load', (event) => {
        var frame = document.createElement("iframe");
        frame.width = "700px"; // Modify to set widget width
        frame.height = "600px"; // Modify to set widget height
        frame.src = "https://widget.lincsproject.ca";
        frame.setAttribute("id", "lincs-widget");                               
        document.body.insertBefore(frame, document.getElementById("lincs-widget-setup"));
                
        frame.addEventListener('load', (event) => {
            frame.contentWindow.postMessage(document.body.innerText, '*');
        });
    });
</script>
```

## WordPress Installation

Start by installing and activating the Code Embed plugin (https://wordpress.org/plugins/simple-embed-code/#description). Next, edit the post or page where you want to add the widget, click on the "Screen Options" button, and check the "Custom Fields" option. Scroll down until you see the "Custom Fields" metabox and click on the "Enter new" button. Under the new field, enter a name with CODE as the prefix (ex. CODEwidget), then paste the following script tag in the value field. Finally, click the "Add Custom Field Button", then add this field to the post or page where you want it to appear.

```
<script id="lincs-widget-setup">
    window.addEventListener('load', (event) => {
        var frame = document.createElement("iframe");
        frame.width = "700px"; // Modify to set widget width
        frame.height = "600px"; // Modify to set widget height
        frame.src = "https://widget.lincsproject.ca";
        frame.setAttribute("id", "lincs-widget");                               
        document.body.insertBefore(frame, document.getElementById("lincs-widget-setup"));
                
        frame.addEventListener('load', (event) => {
            frame.contentWindow.postMessage(document.body.innerText, '*');
        });
    });
</script>
```

## Development

This widget is packaged using Browserify. In order to make changes to the widget, editing should be done in the `/dist/js/lincs-widget-src.js` file. Once the changes are made, the widget need to be packaged with dependencies using the command `browserify lincs-widget-src.js -o lincs-widget.js` from the `/dist/js` directory.

## Deployment

When the development phase is completed, the lincs-widget.js file should be able to be minimized in order to make access as easy/fast as possible.